from aqt.qt import *

class ProgressAndLog(QWidget):

    def __init__(self, filenum, parent=None):
        super().__init__(parent)
        self.warnings = 0
        self.errors = 0
        self.filenum = filenum
        self.progressbar = self.makeProgressBar(filenum)
        self.initUI()
        self.show()

    def makeProgressBar(self, filenum):
        progressbar = QProgressBar(self)
        progressbar.setRange(0,filenum)
        progressbar.setValue(0)
        return progressbar

    def initUI(self):

        self.status = QLabel("reading file")
        self.logtext = QTextEdit()
        self.logtext.setReadOnly(True)
        self.logtext.setLineWrapMode(QTextEdit.NoWrap)
        self.log("starting")

        lo = QVBoxLayout()
        lo.addWidget(self.progressbar)
        lo.addWidget(self.status)
        lo.addWidget(self.logtext)
        self.setLayout(lo)

        self.resize(400,700)

    def setStatus(self,text):
        self.status.setText(text)

    def log(self,text,end="\n",color=QColor(0,0,0)):
        self.logtext.setTextColor(color)
        self.logtext.insertPlainText(text+end)
        sb = self.logtext.verticalScrollBar()
        sb.setValue(sb.maximum())

    def warn(self,text,end="\n"):
        self.warnings += 1
        self.log(text,end,QColor(220,130,10))

    def err(self,text,end="\n"):
        self.errors += 1
        self.log(text,end,QColor(220,0,0))

    def incr_imported(self):
        self.progressbar.setValue(self.progressbar.value() + 1)
        if self.progressbar.value() == self.filenum:
            self.finish()
        # make sure the progress is shown
        QApplication.instance().processEvents()

    def finish(self):
        self.log(f"imported {self.filenum} files with {self.errors} errors and {self.warnings} warnings")

        if (self.errors==0 and self.warnings==0):
            self.setStatus("Done.")
        elif (self.errors==0):
            self.setStatus(f"Done. {self.warnings} warnings")
        else:
            self.setStatus(f"{self.errors} errors and {self.warnings} warnings")
