from enum import Enum
import re

class BlockType(Enum):

    NO_HINTS = 2
    HINT_AND_STEP = 3

class Block():

    def __init__(self,str):

        b = re.split(r"\s+---\s+",str)
        reg = r"\s+#\s+"
        if len(b) == 1:
            self.type = BlockType.NO_HINTS
            self.steps = re.split(reg,b[0])
        elif len(b) == 2:
            self.type = BlockType.HINT_AND_STEP
            self.hints = re.split(reg,b[0])
            self.steps = re.split(reg,b[1])
        else:
            raise ValueError("invalid block ", str)

        self.nextBlock = None

    def __str__(self):

        res = "\n"
        if self.type == BlockType.HINT_AND_STEP:
            attr_list = [str(self.type),str(self.hints),str(self.steps)]
        else:
            attr_list = [str(self.type),str(self.steps)]
        res = res.join(attr_list)
        res += "\n{}".format(str(self.nextBlock))
        return res

    def attachBlock(self,block):

        self.nextBlock = block

    def toHTMLheader(self):
        if self.type != BlockType.NO_HINTS:
            raise ValueError("header cant have hints")
        if len(self.steps) != 1:
            raise ValueError("header can only have one step")
        if self.nextBlock is None:
            raise ValueError("card cant be header only")
        id = generateID()
        text = self.steps[0] + "<br>" + self.nextBlock.toHTML()
        return makeHTMLparagraph(id,text,visible=True)

    def toHTML(self):
        if self.nextBlock is not None:
            nextBlockHTML= self.nextBlock.toHTML()
        else:
            nextBlockHTML= ""
        buttons = []
        texts = []
        if self.type == BlockType.HINT_AND_STEP:
            for i,h in enumerate(self.hints):
                but, text = makeHTMLButtonTextPair("hint" + str(i), h)
                buttons.append(but)
                texts.append(text)
        for i,h in enumerate(self.steps):
            if h==self.steps[-1]:
                but, text = makeHTMLButtonTextPair("step" + str(i), h + "<br>" + nextBlockHTML)
            else:
                but, text = makeHTMLButtonTextPair("step" + str(i), h)
            buttons.append(but)
            texts.append(text)

        res = "\n"
        buttonHTML = res.join(buttons)
        textHTML = res.join(texts)
        res = res.join([buttonHTML, textHTML])
        return res




def generateID(x = [0]):
    x[0] += 1
    return str(x[0])

def chainBlocks(blocklist):
    if len(blocklist) < 2:
        raise ValueError("need more blocks than just one")
    parent = blocklist[0]
    for b in blocklist[1:]:
        parent.attachBlock(b)
        parent = b

def makeHTMLbutton(id_to_reveal, text_on_button):
    return fr"""
<button class="button" type="button" onclick="
        document.getElementById('{id_to_reveal}').style.display=''
">
{text_on_button}
</button>"""

def makeHTMLparagraph(id_of_text, text_to_show, visible=False):
    display = r"''" if visible else "none"
    return fr"""
<div id='{id_of_text}' style="display:{display}">
    {text_to_show}
</div>
    """

def makeHTMLButtonTextPair(text_on_button, text_to_show):
    id = generateID()
    but = makeHTMLbutton(id, text_on_button)
    show = makeHTMLparagraph(id, text_to_show)
    return (but, show)
