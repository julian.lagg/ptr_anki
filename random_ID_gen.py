import random
import time


seeded = False
charlist = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ~!@#%^&*_+1234567890-=?"
random_id_length = 30

def get_random_ID():
    global seeded
    if not seeded:
        seed()
        seeded = True
    id_as_list = [random.choice(charlist) for i in range(random_id_length)]
    return "".join(id_as_list)


def seed():
    random.seed(time.time())


if __name__ == '__main__':

    print(f"with this setup, there are {float(len(charlist) ** random_id_length)} possible IDs")
    print("generating random IDs, test for plausibility:")
    for i in range(5):
        print(get_random_ID())
