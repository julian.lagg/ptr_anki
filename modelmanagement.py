from copy import deepcopy
from aqt import mw

modelnamesuffix = "-With-MathMemoID"

def clone_exists(original):

    d = mw.col
    m = d.models.byName(f"{original}{modelnamesuffix}")
    return m is not None

# if the User Profile is created on a newer Version of Anki,
# the standard Cloze-Card has no "Extra" field.
# this checks wether that is the case
def std_cloze_has_extra():

    d = mw.col
    m = d.models.byName("Cloze")
    if m==None:
        raise ValueError("Your Collection has no 'Cloze'-Type, this is weird.")
    for f in m['flds']:
        if f['name'] == "Extra":
            return True
    return False

def add_extra_field(model):

    # create as copy of first field to get sensible default values
    extrafld = deepcopy(model['flds'][0])
    extrafld['name'] = 'Extra'

    # ord determines the order in which they are read in, it should be
    # id, name, extra, tag.
    # As id is added later, we now create: text then extra

    model['flds'].insert(1, extrafld)

    for f in model['flds']:
        if f['name'] == 'Text':
            f['ord'] = 0
        elif f['name'] == 'Extra':
            f['ord'] = 1
        else:
            raise ValueError(f"unexpected field {f['name']} in add_extra_field")

    # change answer format string, so that the extra is shown
    for t in model['tmpls']:
        t['afmt'] = "{{cloze:Text}}</br></br>{{Extra}}"
    # I don't see why model['tmpls'] should have more than one element,
    # if it does, that probably means there is some semantics that I need to understand
    # so I warn if it is ever more than one element

    if len(model['tmpls']) != 1:
        mw.im_dia.warn(f"tmpls has not one element, but {len(model)['tmpls']}."
                       f"\nThis will probably lead to problems. Please contact me at:"
                       f"\njlagg97@gmail.com")

def create_clone(original):

    d = mw.col
    m = d.models.byName(original)
    idMathNote = d.models.copy(m)

    idMathNote['name'] = f"{original}{modelnamesuffix}"

    # flds are the fields, which is a list of dictionaries,
    # where every dic represents a field. Important members:
    # 'name':str, 'ord':num

    if original=="Cloze" and not std_cloze_has_extra():
        add_extra_field(idMathNote)

    # make room for the new ID-field
    for f in idMathNote['flds']:
        f['ord'] += 1
    idMathNote['flds'].insert(0, deepcopy( idMathNote['flds'][0]))
    idfield = idMathNote['flds'][0]
    idfield['name'] = "ID"
    idfield['ord'] = 0

    print(original, ":::::", idMathNote)
    # the templates for the cards are in 'tmpls', which is a
    # list of cards generated from a note.
    # every card is a dictionary, important members are:
    # afmt, qfmt are answer and question format string.

    # other interesting members of the model dictionary:
    # 'sortf' by what field we sort in the browser (?)
    # this is the first field (ord 0) if I dont change it,
    # and I think that is good, as it keeps blocks together.

def create_models_if_needed():

    for name in ["Basic", "Cloze"]:
        if not clone_exists(name):
            create_clone(name)
