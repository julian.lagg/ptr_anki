from enum import Enum

class ChunkType(Enum):

    NO_HINTS = 1
    HINT_AND_STEP = 2

class Chunk():

    def __init__(self,hints,step):

        if len(hints)==0:
            self.type = ChunkType.NO_HINTS
        else:
            self.type = ChunkType.HINT_AND_STEP

        self.hints = hints
        self.step = step
        self.nextChunk = None

    def attachChunk(self, revealChunk):

        self.nextChunk = revealChunk

    def toHTMLheader(self):

        if self.type != ChunkType.NO_HINTS:
            raise ValueError("header can not have hints")
        if self.nextChunk == None:
            raise ValueError("card cant be header only")

        id = generateID()
        text = "<br/>".join([self.step, self.nextChunk.toHTML()])
        return makeHTMLparagraph(id,text,visible=True)

    def toHTML(self):

        if self.nextChunk is not None:
            nextChunkHTML = self.nextChunk.toHTML()
        else:
            nextChunkHTML = ""

        buttons = []
        texts = []

        for i,h in enumerate(self.hints,start=1):
            but, text = makeHTMLButtonTextPair(f"hint #{i}", h)
            buttons.append(but)
            texts.append(text)

        but,text = makeHTMLButtonTextPair("reveal next step", self.step + nextChunkHTML)
        buttons.append(but)
        texts.append(text)

        buttonHTML = " ".join(buttons)
        textHTML = "\n".join(texts)
        return "\n".join([buttonHTML,textHTML])

def chainChunks(chunklist):

    if len(chunklist) < 2:
        raise ValueError("need more chunks than just one")
    parent = chunklist[0]
    for b in chunklist[1:]:
        parent.attachChunk(b)
        parent = b

def makeHTMLbutton(id_to_reveal, text_on_button):
    return fr"""<button class="button" type="button" onclick="document.getElementById('{id_to_reveal}').style.display=''">{text_on_button}</button>"""

def makeHTMLparagraph(id_of_text, text_to_show, visible=False):
    display = r"''" if visible else "none"
    return fr"""<div id='{id_of_text}' style="display:{display}"> {text_to_show} </div>"""

def makeHTMLButtonTextPair(text_on_button, text_to_show):
    id = generateID()
    but = makeHTMLbutton(id, text_on_button)
    show = makeHTMLparagraph(id, text_to_show)
    return (but, show)


def generateID(x = [0]):
    x[0] += 1
    return str(x[0])
