import re
from copy import copy
from .chunk import Chunk, chainChunks
import itertools
from aqt import mw

from .random_ID_gen import random_id_length

types = ["def","untitleddef","card","theorem"]

# find elements like theorems, cards etc and present them to be iterated over
# returns a list of tuples where each element is (tag,type,text)
# with text being the text inside the triple square brackets
def extractElements(text):

    global types
    elems = []

    for type in types:
        regex = r"ID:(.{" + str(random_id_length) + r"})" + fr"\n\\{type}<<<(([^>]|>[^>]|>>[^>])*)>>>"
        matches = re.findall(regex,text)
        elems.extend(list(map(lambda idcont: (idcont[0], type, idcont[1]),matches)))
        text = re.sub(regex,"",text) # delete the matched part from the string

    if len(elems) > 0:
        mw.im_dia.log(f"extracted {len(elems)} elements")
    else:
        mw.im_dia.warn("extracted no elements")

    return elems, text

# turn \n into <br />
def fixLineBreaks(str):

    regex = r"\n"
    str = re.sub(regex,"<br>",str)
    return str

# split string by lines that contain only a hash
def splitAtHashLine(str):

    regex = r"(?m)^[^\n\S]*#[^\n\S]*$" # match lines with only a hash on them
    ls = re.split(regex,str)

    return ls

def makeProofCard(id_html, generalTags):

    id, proofhtml = id_html

    tags = generalTags + " proof makeProofCard"
    card = ";".join([id, proofhtml," ",tags])

    return card

def simpleFrontToBackCard(id, front, back, tags):

    cardtext = f"{id};{front};<br/>{back};{tags}"
    cardtext = fixLineBreaks(cardtext)
    return cardtext

def makeSimpleCard(id, str, generalTags):

    tags = generalTags + " other makeSimpleCard"

    ls = splitAtHashLine(str)

    if len(ls) != 2:
        mw.im_dia.warn(f"Invalid simple card:\n{str}")

    return simpleFrontToBackCard(id, ls[0],ls[1],tags)

def makeProofHTML(header,answer):
    hintregex = proofatomregex(r"\-")
    stepregex = proofatomregex(r"\+")

    steps_with_hints = []
    last_match_end = 0
    for match in re.finditer(stepregex, answer):
        step = match.group(1) # get what was caught by the group
        hints_raw = answer[last_match_end:match.start()]
        hints = re.findall(hintregex, hints_raw)
        last_match_end = match.end()
        steps_with_hints.append((hints, step))

    headerchunk = Chunk([],header)
    chunklist = [headerchunk]

    for hints, step in steps_with_hints:
        chunk = Chunk(hints, step)
        chunklist.append(chunk)

    chainChunks(chunklist)

    html = headerchunk.toHTMLheader()
    html = fixLineBreaks(html)
    return html


# gives a regex that matches everything from including start-regex to excluding ?-+ or end.
# captures the part between startregex and end
def proofatomregex(start):
    return rf"(?m){start}((?:.*\n)*?)(?=^[-+?]|$)"

def makeProofList(id, text, generalTags):

    tags = generalTags + " proof makeProofList"

    # give every card a unique id that is a spin-off of the original id of that card
    idhead = id[0:random_id_length-5] + "prf"

    ls = splitAtHashLine(text)

    if len(ls) != 3:
        raise ValueError(f"Invalid theorem:\n {ls}")
    if not re.match(r"\s*\?\?\?",ls[2]):
        # no proofs found, but proofs are not mandatory
        return []

    ls[1] = makePlainTextFromOmissions(ls[1])
    generalheader = "<br/>".join(ls[0:2])
    generalheader = fixLineBreaks(generalheader)

    quesregex = proofatomregex(r"\?\?\?")

    # doing it like this has some implications on what whitespace you can use.
    # I don't precisely know what, but it works for now
    qa_plain_list = re.split(quesregex,ls[2])
    questions = qa_plain_list[1::2]
    answers = qa_plain_list[2::2]
    qa = zip(questions, answers)

    id_proofs = []

    for count, (q,a) in enumerate(qa):
        id = idhead + str(count).zfill(2)
        assert len(id) == random_id_length
        header = "<br/>".join([generalheader,q])
        html = makeProofHTML(header,a)
        id_proofs.append((id, html))


    return id_proofs

# a list of cloze cards generated from an untitleddef item
# gets the generalTags and " definition" as tag
def clozeListFromUntitledDef(id,text,generalTags):

    tags = generalTags + " definition clozeListFromUntitledDef"

    elems = splitAtHashLine(text)

    if len(elems) == 2:
        extra = elems[1]
    else:
        extra = ""

    core = elems[0]

    id_cards = makeClozes(id,core)

    # add tail (extra & tags) to every card
    # as a last step, fix all newlines to html linebreaks
    tail = fr";<br/>{extra};{tags}"
    cardList = list(map(fixLineBreaks, map(lambda id_c : id_c[0] + ";" + id_c[1] + tail, id_cards)))

    return cardList


def clozeListFromDef(id, text, generalTags):

    tags = generalTags + " definition clozeListFromDef"
    elems = splitAtHashLine(text)

    if len(elems) == 3:
        extra = elems[2]
    else:
        extra = ""

    core = elems[1]
    id_card = makeClozes(id,core)
    head = fr"<br/>{elems[0]}<br/>"
    tail = fr";<br/>{extra};{tags}"
    cardList = list(map(fixLineBreaks, map(lambda id_c: id_c[0] + ";" + head + id_c[1] + tail, id_card)))

    return cardList

def basicListFromDef(id,text, generalTags):

    tags = generalTags + " definition basicListFromDef"
    elems = splitAtHashLine(text)

    if len(elems) == 3:
        extra = elems[2]
    else:
        extra = ""

    plainDef = makePlainTextFromOmissions(elems[1])

    idhead = id[0:random_id_length-5]
    ntdefid = idhead + "ntdef"
    deftnid = idhead + "deftn"

    nameToDef = simpleFrontToBackCard(ntdefid, elems[0], f"{plainDef}\n\n{extra}", tags)
    defToName = simpleFrontToBackCard(deftnid, f"name the definition:\n\n{plainDef}", f"{elems[0]}\n\n{extra}", tags)
    return [nameToDef,defToName]

def makeBasicCardsFromTheorem(id,text, generalTags):

    tags = generalTags + " theorem makeBasicCardsFromTheorem"
    elems = splitAtHashLine(text)

    plainStatement = makePlainTextFromOmissions(elems[1])

    idhead = id[0:random_id_length-5]
    ntsid = idhead + "ntsta"
    stnid = idhead + "stnam"

    nameToStatement = simpleFrontToBackCard(ntsid,elems[0], plainStatement, tags)
    statementToName = simpleFrontToBackCard(stnid,"name the theorem:\n\n"+plainStatement, elems[0], tags)
    return [nameToStatement,statementToName]

def makeClozesFromTheorem(id, text, generalTags):

    tags = generalTags + " theorem makeClozesFromTheorem"
    elems = splitAtHashLine(text)

    statement = elems[1]
    id_cardFrontList = makeClozes(id,statement)

    head = fr";<br/>{elems[0]}<br/>"
    tail = fr";;{tags}" # extra is left empty
    cardList = list(map(fixLineBreaks, map(lambda id_card: id_card[0]+ head+id_card[1]+tail, id_cardFrontList)))

    return cardList


# match [text] and even [\n], but not []. gives two matches on "[1] [2]"
# because of the lazy quantifier +?.
# this gives only one match on [ [$] \LaTeX [/$] ]
# this does not match on [$] and [/$]
omissionFinderRegex = r"\[(?!\$\]|/\$)([\s\S]+?)(?<!\$)\]"


def makePlainTextFromOmissions(text):

    regex = omissionFinderRegex

    replace = lambda m : m.group(1)
    return re.sub(regex, replace, text)

# takes a text with ommisions in []-brackets and returns the front of possible cloze cards
# as a list, that is, for every omission make a card where only that one is left out
# and make a card where they are all left out
def makeClozes(id, text):

    regex = omissionFinderRegex

    replace = lambda m, c=itertools.count(1) : "{{c" + str(next(c)) + "::" + str(m.group(1)) + "}}"
    singles, n = re.subn(regex,replace,text)
    if n==0:
        mw.im_dia.warn(f"No Omissions found on:\n{text}")

    replace = lambda m : "{{c1::" + str(m.group(1)) + "}}"
    multi = re.sub(regex,replace,text)

    idhead = id[0:random_id_length-5]
    idsingle = idhead + "singl"
    idmulti  = idhead + "multi"

    return [(idsingle, singles), (idmulti, multi)]
