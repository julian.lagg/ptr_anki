import re
import itertools
# dont call sys.exit!!
# import sys
from aqt import mw

def printTraceback():
    # this somehow causes anki to kill my addon
    try:
        import traceback
        print(traceback.print_exc())
    except:
        print("failed to print traceback")

def shortenFileName(name):
	reg = f"(?<=/)[^/]*$"
	return re.search(reg, name).group(0)

def removeFileEnding(filename):

     regex = r"\..*$"
     return re.sub(regex,"",filename)

# convert "text $ math $ text" into "text [$] math [/$] text"
def convertDollars(str):

    # converts none to empty string, is otherwise identity operation
    def noneToEmpty(str):
        if str==None:
            return ""
        return str

    # replace $ pairs with [$] [/$]
    # but replace [$math$] to [[$]math[/$]]
    # matches and captures one character before the $
    # regex = r"(?:^|(?!\[\$\]|/\$\]|\\\$)(.))\$"
    regex = r"(?m)(?:(?!\[\$\]|/\$\]|\\\$)(.))\$|^\$"
    replace = lambda m, rep=itertools.cycle([r"[$]",r"[/$]"]) : noneToEmpty(m.group(1))+next(rep)
    str,n = re.subn(regex,replace,str)
    if n%2 != 0:
        mw.im_dia.warn("matched an uneven number of $-signs for Latex-equation-env")

    # replace \$ with $ (but [/$] and [$] stays, all $ stay)
    regex = r"\\\$"
    str = re.sub(regex,"$",str)

    return str


def ignoreComments(str):
    regex = r"//[^\n]*"
    return re.sub(regex,"",str)

def preprocess(str):

    data = ignoreComments(str)
    data = convertDollars(data)
    data = data.lstrip() # remove leading whitespace

    return data


def extractTags(str):
    tagline,_,str= str.partition("\n") # read first line
    _, _, tags = tagline.partition("tags:")
    if tags == "":
        mw.im_dia.warn("first non-comment non-blank line must specify tags")

    return tags, str

def clearBlankLines(str):
    regex = r"(?m)^\s*\n"
    return re.sub(regex,"",str)

def warnIfNotEmpty(str):

    str = clearBlankLines(str)

    if str != "":
        mw.im_dia.warn(f"could not match entire input. Still unmatched: {str}")
    else:
        mw.im_dia.log("Parsing successfull")
