from shutil import copy

def make_backup(file):

    fileparts = file.split("/")
    path = "/".join(fileparts[:-1])
    oldname = fileparts[-1]
    newname = "backup_of_" + oldname
    backupfile = f"{path}/{newname}"

    copy(file, backupfile)
