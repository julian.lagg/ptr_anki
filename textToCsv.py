from .util import *
from .elements import *
import sys
import os
from aqt import mw
from .syntaxchecker import warn_if_bad_math

def csvDeckFromList(ls, type):

    cardnum = len(ls)
    deck = "\n".join(ls)
    deck += "\n\n"

    filename = f"{type}.deck"

    with open(filename, "w") as out:
        out.write(deck)

    mw.im_dia.log(f"created {cardnum} {type} notes")

    return filename

def textFileToCsvDeck(filename):

    with open(filename,"r") as file:
        data = file.read()

    data = preprocess(data)

    warn_if_bad_math(data)

    tags, data = extractTags(data)
    tags += " " + shortenFileName(filename)
    mw.im_dia.log(f"using Tags:\t{tags}")

    elems, unmatched = extractElements(data)

    warnIfNotEmpty(unmatched)

    basicdeck = []
    clozedeck = []


    for id,type,text in elems:

        if type == "card":
            card = makeSimpleCard(id,text,tags)
            basicdeck.append(card)
        elif type == "theorem":
            id_proofs = makeProofList(id,text,tags)
            for idp in id_proofs:
                card = makeProofCard(idp,tags)
                basicdeck.append(card)
            clozeCards = makeClozesFromTheorem(id,text,tags)
            clozedeck.extend(clozeCards)
            basicCards = makeBasicCardsFromTheorem(id,text,tags)
            basicdeck.extend(basicCards)
        elif type == "untitleddef":
            clozecards = clozeListFromUntitledDef(id,text,tags)
            clozedeck.extend(clozecards)
        elif type == "def":
            clozecards = clozeListFromDef(id,text,tags)
            clozedeck.extend(clozecards)
            basiccards = basicListFromDef(id,text,tags)
            basicdeck.extend(basiccards)
        else:
            mw.im_dia.warn(f"unknown type {type}, no card generated from that block")

    basicfile = csvDeckFromList(basicdeck, "basic")
    clozefile = csvDeckFromList(clozedeck, "cloze")

    return basicfile, clozefile
