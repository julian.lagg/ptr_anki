from util import *
from elements import *
import sys
import os
from syntaxchecker import warn_if_bad_math

if len(sys.argv) == 2:
    msfilename = sys.argv[1]
    shouldOpenEditor = True
elif len(sys.argv) == 3 and sys.argv[2] == "-no-show":
    msfilename = sys.argv[1]
    shouldOpenEditor = False
else:
    print("usage: python3 mstodeck.py <filename>")
    sys.exit()

with open(msfilename,"r") as file:

    data = file.read()

data = preprocess(data)
warn_if_bad_math(data)

tags, data = extractTags(data)
tags += " " + msfilename
print("using Tags:\t", tags)

elems, data = extractElements(data)

warnIfNotEmpty(data)

basicdeck = []
clozedeck = []

for type,text in elems:

    if type == "card":
        card = makeSimpleCard(text,tags)
        basicdeck.append(card)
    elif type == "theorem":
        proofs = makeProofList(text,tags)
        for p in proofs:
            card = makeProofCard(p,tags)
            basicdeck.append(card)
        clozeCards = makeClozesFromTheorem(text,tags)
        clozedeck.extend(clozeCards)
        basicCards = makeBasicCardsFromTheorem(text,tags)
        basicdeck.extend(basicCards)
    elif type == "untitleddef":
        clozecards = clozeListFromUntitledDef(text,tags)
        clozedeck.extend(clozecards)
    elif type == "def":
        clozecards = clozeListFromDef(text,tags)
        clozedeck.extend(clozecards)
        basiccards = basicListFromDef(text,tags)
        basicdeck.extend(basiccards)
    else:
        print("WARNING: unknown type ", type, ", no card generated from that block!")


basiccardnum = len(basicdeck)
basicdeck = "\n".join(basicdeck)
basicdeck = basicdeck + "\n\n"

clozecardnum = len(clozedeck)
clozedeck = "\n".join(clozedeck)
clozedeck = clozedeck + "\n\n"

if basiccardnum > 0:
    outfilename= f"{removeFileEnding(msfilename)}_basic.deck"
    with open(outfilename, "w") as out:

        out.write(basicdeck)
        print(f"created {outfilename} with {basiccardnum} cards")
    if shouldOpenEditor:
        os.system(f"gedit {outfilename} &")
else:
    print("no basic cards to generate, did not create a deck for basic cards")

if clozecardnum > 0:
    outfilename= f"{removeFileEnding(msfilename)}_cloze.deck"
    with open(outfilename, "w") as out:

        out.write(clozedeck)
        print(f"created {outfilename} with {clozecardnum} cards")
    if shouldOpenEditor:
        os.system(f"gedit {outfilename} &")
else:
    print("no cloze cards to generate, did not create a deck for cloze cards")

if clozecardnum==0 and basiccardnum==0:
    print("WARNING: no cards generated")

print(f"generated {basiccardnum+clozecardnum} cards in total")
