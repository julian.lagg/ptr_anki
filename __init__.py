# import the main window object (mw) from aqt
from aqt import mw
# import the "show info" tool from utils.py
from aqt.utils import showInfo
# import all of the Qt GUI library
from aqt.qt import *
from anki.importing import TextImporter
from .importdialog import ProgressAndLog

from .textToCsv import textFileToCsvDeck
from .util import shortenFileName
from .IDadder import simpleFileReport, addMissingIDs, FileEvaluation
from .filehandling import make_backup
from .modelmanagement import create_models_if_needed, modelnamesuffix

import os
from anki.utils import isWin, isLin, isMac

devemail = "jlagg97@gmail.com"

def getFiles():
    files = QFileDialog().getOpenFileNames()
    # first item of the tuple is a list of all the selected files
    return files[0]

def csvToCards(file, deckid, type):

    create_models_if_needed()

    # select deck
    mw.col.decks.select(deckid)

    # anki defaults to the last note type used in the selected deck, we don't want that
    m = mw.col.models.byName(type)
    if m is None:
        raise ValueError(f"type {type} is not in the Collection.")

    deck = mw.col.decks.get(deckid)
    deck['mid'] = m['id']
    mw.col.decks.save(deck)
    # and puts cards in the last deck used by the note type
    m['did'] = deckid
    # import into the collection
    ti = TextImporter(mw.col, file)
    
    #ti.importMode = UPDATE_MODE # redundant for now, but just to be sure

    # first field is id, which should be used for uniqueness-determination but not put on cards
    # the "not putting ID on cards" part of the job is handled by the note-type, not the importer.
    if type == f"Basic{modelnamesuffix}":
        ti.mapping = ['ID', 'Front', 'Back', '_tags']
    elif type == f"Cloze{modelnamesuffix}":
        ti.delimiter = ";"
        ti.mapping = ['ID', 'Text', 'Extra', '_tags']
    else:
        raise ValueError(f"unexpected type {type} in csvToCards")

    ti.allowHTML = True
    ti.initMapping()
    ti.run()

def logReport(evaluation, report,shname):

    if evaluation == FileEvaluation.REPORT_INCOMPLETE:
        # python3.7 dicts or newer are officially ordered
        # the last logged error in report was the reason for termination
        line_num, reason = report.items()[-1]
        mw.im_dia.err(f"syntax error found on file: {shname}\nIn line {line_num}: {reason}")
    elif evaluation == FileEvaluation.ALL_CORRECT: # report is empty
        mw.im_dia.log(f"file {shname} has all IDs set up")
    elif evaluation == FileEvaluation.MORE_PROBLEMS:
        mw.im_dia.warn(f"file {shname} has minor problems:")
        for line_num, reason in report.items():
            mw.im_dia.warn(f"in line {line_num}: {reason}")
    elif evaluation == FileEvaluation.MISSING_IDS:
        mw.im_dia.log(f"Automatically adding IDs. The File with missing IDs has been saved.")
    else:
        raise ValueError(f"Unknown evaluation value in logReport: {evaluation}, this should never happen")

def importMath():

    files = getFiles()
    deckid = mw.col.decks.id("new math imports")
    mw.im_dia = ProgressAndLog(len(files))

    if isMac:
        mw.im_dia.warn("You seem to be running MacOS.\n"
                       "this addon is untested on Mac, please report problems to\n"
                       f"{devemail}")
    else:
        mw.im_dia.log("This addon is quite new and might have problems\n"
                      "please report them to\n"
                      f"{devemail}")

    for file in files:
        shname = shortenFileName(file)
        mw.im_dia.log(f"file: {shname}")
        mw.im_dia.setStatus(f"file: {shname}")
        evaluation, report = simpleFileReport(file)
        logReport(evaluation,report,shname)

        if evaluation == FileEvaluation.MISSING_IDS:
            make_backup(file)
            addMissingIDs(report, file, file)
            evaluation = FileEvaluation.ALL_CORRECT

        if evaluation == FileEvaluation.ALL_CORRECT:
            try:
                basic_csv, cloze_csv = textFileToCsvDeck(file)
                csvToCards(basic_csv,deckid,f"Basic{modelnamesuffix}")
                csvToCards(cloze_csv,deckid,f"Cloze{modelnamesuffix}")
                mw.im_dia.log(f"{shname} done")
            except Exception as e:
                mw.im_dia.err(f"failure on file: {shname}\n{repr(e)}")
        mw.im_dia.incr_imported()
    mw.reset()

# create a new menu item, "test"
importAction = QAction("import m&ath", mw)
# set it to call testFunction when it's clicked
importAction.triggered.connect(importMath)
# and add it to the tools menu
mw.form.menuTools.addAction(importAction)


########## help ################

def openExample():


    if isLin or isMac:
        fileparts = __file__.split("/")
        example_loc = "/".join(fileparts[0:-1]) + "/example.amm"
        if "EDITOR" in os.environ:
            editor = os.environ['EDITOR']
        else:
            editor = "gedit"
        os.system(f"{editor} {example_loc} &")
    elif isWin:
        fileparts = __file__.split("\\")
        example_loc = "\\".join(fileparts[0:-1]) + "\\example.amm"
        os.system(f"notepad {example_loc}")


help = QAction("show example anki-math-memo file", mw)
help.triggered.connect(openExample)
mw.form.menuHelp.addAction(help)
