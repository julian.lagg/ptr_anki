// This file is an example of how to use MathMemo for anki to learn proofs and theorems using Anki.
// create files like this with any text editor and share/save them with any version control independent of anki.

// This addon is still in development, please report any problems to me: jlagg97@gmail.com

//To create flashcards from a mathmemo-textfile like this, import it to anki by selecting Tools>"import math" from the menubar on the top.
//The generated cards will appear in a deck called "new math imports", so you can have a look at them to check if they are all correct before copying them into your study deck.

//coments start with //, like in C.


tags: example mathmemo other
//the first non-coment-non-blank-line defines (possibly multiple) tags that all cards generated from this file will receive
//tags are useful for organizing your cards, suggestions are topic and chapter name or number etc.
//cards additionaly get tags based on whether they are a definition, theorem etc.



//a simple flashcard with back and front, a '#' character seperates front and back:
ID:LH_fe!IoN@L&@^ziT_Hud^Qd@2-Z_M
\card<<<
question
#
//comments can be in cards, they will not appear on the card
answer //coments also work when they are not on their own line
>>>
//simple cards are tagged as "other"
//you can't use triple closing ">" on a card. The technology isn't there yet.

//you may have noticed there is an ID-field in front of the block.
//You do not need to add it yourself when creating cards, it gets added automatically by the addon on first import
//The point of these is that when you edit this file, say, correct an error on a card, anki recognizes that this is an update to the old card
//rather than a new card. This autoadding modifies your file. For safety, your original file gets saved as a backup (though I am confident you won't need it)

//You can use Latex to show some math on your cards. For now, packages amssymb and amsmath are used which covers standard stuff.
//pairs of $ and $ are converted to [$] and [/$], the anki way to make a latex equation environment
//you may use [$] and [/$] yourself if you are good at text editors
ID:sPpq61~se-QyIxbmaQ1yU9VK-Yd=zb
\card<<<
$ \pi^2 = $
#
$ 9.81 \frac{m}{s^2} $
>>>
// to use Latex on cards, you need to have a Latex-compiler on your computer and anki needs to find it.
// if you are able to create a card with latex on it using the gui of regular anki, it should also work with this.
// once created, a card can be viewed on other devices (including mobile) or the browser, independent of whether Latex or this addon is known on the device

// when your Latex is wrong, the program will try to warn you, but no guarantees.
ID:VyIcfciEighbgTXmficW%CIsfxNNJ?
\card<<<
$ \sqrrrt{2} = 1.414... $
#
there should be a warning when importing this.
>>>

//to write a dollar sign, # etc., escape it with backslash
ID:@5KP7e8NbcF-ZYssvK5PWgYS1&_@zQ
\card<<<
currency of the US?
#
\$
>>>

//for definitions it may be useful to have a certain kind of overlapping cloze:
//a title and an explanation with cloze deletions.
ID:L75Wd9b5cOph5B~DuGPz0vGs6ejXri
\def<<<
title
#
some text where some [parts] are [omitted]
latex must be [$completely inside$] or $completely outside$ the [omissions]
>>>
//this example produces 4 flashcards where the title and all but one omission is shown,
//one where all omissions are hidden
//one where the text is shown and the title must be guessed
//one where only the title is shown and the entire definition must be guessed
//all cards will be tagged with "definition"


//to make the same thing without the title (and the cards associated with it) use untitleddef:
ID:7DpM82qvLc1CBsWkM8k+&@mQ%tv~A?
\untitleddef<<<
some [text] without a [title]
>>>
//these cards will also be tagged with "definition"

//both defs and untitleddefs can have a third field. This will be shown as an extra.
ID:N1B8wKq_NMRmgDhziK7c?Q=K2cUz0W
\untitleddef<<<
another [definition] with no [title] but some [extra]-stuff
#
the extra stuff has no omissions. It will be revealed with the answer on every created card.
>>>
// the extra field is useful for adding some additional information to a card without making it part of what needs to be guessed
// for example, you may want to include a source or a clarification without making it part of what needs to be guessed on the card.

ID:G0WVo^mSBZ5rEpl=aRR5ePKm9VNIT+
\def<<<
titled definition with an extra
#
definition with [possibly] [multiple] [omissions]
#
extra
>>>

//A theorem is a named statement with a proof.
//The statement works just like a definition and generates the same cards, but tagged as "theorem"
//After the statement multiple questions with partially revealing answers may be added.
ID:j0znNMOL%gmQ0fnrW_cl^ghw0@Stlk
\theorem<<<
example-lemma
#
statement of the lemma, possibly with some [omissions] and $ \pi $
#
// The proof(s) go here. What is behind the triple question mark gets shown as a prompt when you first see the card.
// the steps are behind + signs, between steps you may add hints behind a - sign. They are to be understood as hints for the next step.
??? prove uniqueness!
- a hint for the first step
- another hint
- hints and steps can use Latex, too. $ \pi $
+ the actual steps of the proof are behind a plus sign
+ there may be multiple steps
- and there may be hints associated with later steps
+ the last step concludes the proof
??? prove existence!
//the idea is that one theorem can have multiple questions.
//you may not want to double the statement of fact to achieve that.
+ a proof must have at least one step.
>>>

// The cards that contain the proof of the theorems are tagged as "proof"

// having a theorem without a proof is also possible:
ID:n1?B9H5O@G-*ln-ViB9g&vocpX?XEj
\theorem<<<
name of result from advanced theory
#
statement of fact with [omissions] as usual
#
proof-sketch / excuse / source / etc.
>>>
// no card asking for a proof will be generated.
// the cards are tagged as "theorem" as usual
// upon reveal, the cards will not only show the answer but also the third field.
// However, as of now, the third field is not optional (though it may be empty)