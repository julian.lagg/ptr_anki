from enum import Enum, IntEnum
import re

from .random_ID_gen import get_random_ID



def getLineType(str):

    if str.startswith("//"):
        return Linetype.COMMENT
    elif str.startswith("ID:"):
        return Linetype.ID_LINE
    elif re.match(r"^\S+<<<", str):
        return Linetype.START_OF_BLOCK
    elif re.match(r"^[^\n]*>>>[^\n]*$", str):
        return Linetype.END_OF_BLOCK
    else:
        return Linetype.NORMAL_LINE

class Linetype(Enum):
    NORMAL_LINE = 0
    END_OF_BLOCK = 1
    START_OF_BLOCK = 2
    ID_LINE = 3
    COMMENT = 4

class ParserState(Enum):
    INSIDE_BLOCK = 0
    OUTSIDE_BLOCK = 1
    ID_SEEN = 2

class FileProblem(Enum):
    BLOCKSTART_WITHOUT_ID = 0
    DOUBLE_ID = 1
    ILLEGAL_BLOCKSTART = 2
    ILLEGAL_BLOCKEND = 3

    def __str__(self):
        if self==FileProblem.BLOCKSTART_WITHOUT_ID:
            return "no ID before Block"
        elif self==FileProblem.DOUBLE_ID:
            return "more than one ID before a Block"
        elif self==FileProblem.ILLEGAL_BLOCKSTART:
            return "unexpected <<<, cannot start a block there"
        elif self==FileProblem.ILLEGAL_BLOCKEND:
            return "unexpected >>>, cannot end  a block there"
        else:
            raise ValueError(f"Fileproblem has unknown value {self}")

class FileEvaluation(IntEnum):
    ALL_CORRECT = 0
    MISSING_IDS = 1
    MORE_PROBLEMS = 2
    REPORT_INCOMPLETE = 3

def simpleFileReport(file):

    # returns FileEvaluation, report(dic of line-number and error)

    state = ParserState.OUTSIDE_BLOCK
    report = {}
    evl = FileEvaluation.ALL_CORRECT
    with open(file, "r") as file:

        for line_num,line in enumerate(file):

            linetype = getLineType(line)

            if linetype in [Linetype.NORMAL_LINE,Linetype.COMMENT]:
                continue

            # if only python had tail recursion

            if state == ParserState.OUTSIDE_BLOCK:
                if linetype == Linetype.START_OF_BLOCK:
                    # missing ID, but can continue normally
                    state = ParserState.INSIDE_BLOCK
                    report[line_num] = FileProblem.BLOCKSTART_WITHOUT_ID
                    evl = max(evl, FileEvaluation.MISSING_IDS)
                    continue
                elif linetype == Linetype.END_OF_BLOCK:
                    # finding an end of block while outside a block is a fatal error
                    report[line_num] = FileProblem.ILLEGAL_BLOCKEND
                    evl = FileEvaluation.REPORT_INCOMPLETE
                    return evl, report
                elif linetype == Linetype.ID_LINE:
                    state =  ParserState.ID_SEEN
                    continue
                else:
                    raise ValueError("this should never happen #1")

            elif state == ParserState.ID_SEEN:
                if linetype == Linetype.START_OF_BLOCK:
                    state = ParserState.INSIDE_BLOCK
                    continue
                elif linetype == Linetype.END_OF_BLOCK:
                    # this is an error
                    report[line_num] = FileProblem.ILLEGAL_BLOCKEND
                    evl = FileEvaluation.REPORT_INCOMPLETE
                    return evl, report
                elif linetype == Linetype.ID_LINE:
                    # this error is not fatal
                    report[line_num] = FileProblem.DOUBLE_ID
                    evl = max(evl, FileEvaluation.MORE_PROBLEMS)
                    continue
                else:
                    raise ValueError("this should never happen #2")

            elif state == ParserState.INSIDE_BLOCK:
                if linetype == Linetype.ID_LINE:
                    # An ID-line inside a block is assumed to be card content
                    continue
                elif linetype == Linetype.END_OF_BLOCK:
                    state = ParserState.OUTSIDE_BLOCK
                    continue
                elif linetype == Linetype.START_OF_BLOCK:
                    # block inside a block is an error
                    report[line_num] = FileProblem.ILLEGAL_BLOCKSTART
                    evl = max(evl, FileEvaluation.MORE_PROBLEMS)
                    return evl, report
                else:
                    raise ValueError("this should never happen #3")

            else:
                raise ValueError("this should never happen #4")

    return evl, report

def addMissingIDs(report, filein, fileout):

    with open(filein, "r") as filein:
        fulltext = filein.read()
        all_lines = fulltext.splitlines()

    for offset, (line_num, error) in enumerate(report.items()):

        if error != FileProblem.BLOCKSTART_WITHOUT_ID:
            raise ValueError("add missing IDs called on File with errors other than missing IDs")

        newIDline = f"ID:{get_random_ID()}"

        all_lines.insert(offset+line_num, newIDline)

    text_with_ID = "\n".join(all_lines)


    # "w" clears the file before writing
    with open(fileout, "w") as out:
        out.write(text_with_ID)







if __name__ == '__main__':
    with open("example.amm", "r") as file:
        print(simpleFileReport(file))

