# this duplicates a lot of functionality from github.com/ankitects/anki/blob/master/pylib/anki/latex.py#L111
# if this breaks check what changed

import re
import os

from aqt import mw
from aqt.qt import QApplication

import anki
from anki.utils import call, namedtmp, tmpdir

import sys

# taken from github.com/ankitects/anki/blob/master/pylib/anki/latex.py
latexCommand = ["latex", "-interaction=nonstopmode", "tmp.tex"]


# checks if everything inside [$] ... [/$] is correct Latex
def warn_if_bad_math(str):

    rex = r"\[\$\]([\s\S]*?)\[\/\$\]"
    eqs = re.findall(rex,str)

    mw.im_dia.log("checking Latex-Syntax")

    allmath = r"\\".join(eqs)

    if equation_is_correct(allmath):
        mw.im_dia.log("no Latex errors found")

    else:
        mw.im_dia.log("locating latex error:")
        mw.im_dia.log("\u25A1"*len(eqs))
        bad_eqs = []
        for eq in eqs:
            if not equation_is_correct(eq):
                bad_eqs.append(eq)
                mw.im_dia.log("\u25A0", end="")
            else:
                mw.im_dia.log("\u25A1", end="")
            QApplication.instance().processEvents()
        mw.im_dia.log("\n")
        report = locate_equations(bad_eqs, str)
        logLatexReport(report)

    return

# prints a Latex error report [(line_desc, eq)] as returned by locate_equations to the import dialog
def logLatexReport(report):

    maxshow = 8 # show no more errors than this amount

    disclaimer = "" if len(report) <= maxshow else f"(showing first {maxshow} of {len(report)})"

    mw.im_dia.warn(f"{len(report)} latex equations seem erroneous {disclaimer}\n")
    for line_desc, eq in report[:maxshow]:

        mw.im_dia.warn(f"In {line_desc}:\n{eq}\n")


# returns a list of tuples (line_nums, equation) with equations believed to be wrong in the text
# eqs is all the bad equations found in the text (in order of appearance)
def locate_equations(eqs, text):

    res = []

    # store lines
    lines = text.splitlines()

    # for each equation, store how many lines it spans
    eqs_linecount = list(map(lambda eq: (eq, 1+eq.count("\n")), eqs))

    fl = 0
    while eqs_linecount:

        eq, eql = eqs_linecount.pop(0)

        while True:
            # check if lines [fl,ll) contain the equation eq
            ll = fl + eql
            cand = "\n".join(lines[fl:ll])
            if eq in cand:
                res.append((line_nums_to_string(fl,ll), eq))
                break
            else:
                fl += 1

    return res

def line_nums_to_string(first, afterlast):

    last = afterlast - 1

    if last == first:

        return f"line {first}"

    return f"lines {first}-{last}"






latex_pre = r"""
\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{mathtools}
\usepackage{amsmath}
\usepackage{amssymb}

\begin{document}

\begin{align}
"""

latex_post = r"""
\end{align}
\end{document}
"""

def equation_is_correct(latex_str):

    filepath = namedtmp("tmp.tex")
    with open(filepath, "w", encoding="utf8") as texfile:
        texfile.write(latex_pre + latex_str + latex_post)

    oldcwd = os.getcwd()
    os.chdir(tmpdir())

    try:
        with open(os.devnull, "w") as devnull:
            return not call(latexCommand, wait=True, stdout=devnull, stderr=sys.__stderr__)

    except Exception as  e:
        mw.im_dia.err(f"The following error occurred trying to execute Latex:\n{e}")

    finally:
        os.chdir(oldcwd)

    return True
